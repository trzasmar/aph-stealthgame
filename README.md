# Dark action stealth cooperative role playing game by Marek Trzaskalik

Marek Trzaskalik (trzasmar), Aleš Répáš (repasale)

Game Design Document: 

https://app.milanote.com/1QYcFf1yZHHefd?p=Ct2UDrBx2J9

How to play:

The game is playable on LAN. In order to play, download [Eclipse of Light](https://drive.google.com/file/d/1HGNSFYf1AhsXxq0Qdl0lJsBHOaGb1NK7/view?usp=sharing) navigate to "APHStartGame.exe" in "Eclipse of Light" folder and run in. Then the server has to click "create session" and the client has to spam-click "join session" for some reason. The authentication and session control is not yet
implemented as it was not part of NI-APH requirements. 

Build steps:

The game is currently buildable only in Unreal Engine and it is missing important networking component which is reponsible for authenticating and connecting players and creating sessions.
Therefore in order to play this game, use Unreal Engine 5.3.2 and launch it with two players or build it and connect to your local machine session.


