// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "APHStealthGameGameMode.generated.h"

UCLASS(minimalapi)
class AAPHStealthGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAPHStealthGameGameMode();
};



