// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class APHStealthGame : ModuleRules
{
	public APHStealthGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "OnlineSubsystem", "OnlineSubsystemEOS", "OnlineSubsystemUtils"});

        PrivateDefinitions.Add("P2PMODE=1");
    }
}
