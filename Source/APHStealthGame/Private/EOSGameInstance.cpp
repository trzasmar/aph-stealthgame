// Fill out your copyright notice in the Description page of Project Settings.


#include "EOSGameInstance.h"
#include "Interfaces/OnlineFriendsInterface.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "OnlineSubsystemUtils.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Kismet/GameplayStatics.h"


const FName TestSessionName = FName("Test Session");


UEOSGameInstance::UEOSGameInstance()
{

}

void UEOSGameInstance::Init() {

	Super::Init();  

	IOnlineSubsystem* Subsystem = Online::GetSubsystem(this->GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();
	isInSession = false;

	this->SessionInviteDelegateHandle = Session->AddOnSessionUserInviteAcceptedDelegate_Handle(
		FOnSessionUserInviteAccepted::FDelegate::CreateUObject(
			this, &UEOSGameInstance::OnSessionUserInviteAccepted));

}

void UEOSGameInstance::SetNotificationHandles() {

	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();

	ConnectToSessionHandle = Session->AddOnSessionParticipantsChangeDelegate_Handle(
		FOnSessionParticipantsChangeDelegate::CreateUObject(
			this, &UEOSGameInstance::HandleConnectToSessionCompleted));
}

void UEOSGameInstance::CreateSession(bool isLAN)
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(this->GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();

	this->CreateSessionDelegateHandle =
		Session->AddOnCreateSessionCompleteDelegate_Handle(FOnCreateSessionComplete::FDelegate::CreateUObject(
			this,
			&UEOSGameInstance::HandleCreateSessionComplete));

	TSharedRef<FOnlineSessionSettings> SessionSettings = MakeShared<FOnlineSessionSettings>();


	SessionSettings->NumPrivateConnections = 1;
	SessionSettings->NumPublicConnections = 0;
	SessionSettings->bShouldAdvertise = false;
	SessionSettings->bUsesPresence = true;
	SessionSettings->bAllowInvites = true;
	SessionSettings->bUseLobbiesIfAvailable = false;
	

	SessionSettings->Settings.Add(FName(TEXT("SessionSetting")),FOnlineSessionSetting(FString(TEXT("SettingValue")), EOnlineDataAdvertisementType::ViaOnlineService));

	// Create a session and give the local name "MyLocalSessionName". This name is entirely local to the current player and isn't stored in EOS.
	if (!Session->CreateSession(0, FName(TEXT("MyLocalSessionName")), *SessionSettings))
	{
		// Call didn't start, return error.
	}
}

void UEOSGameInstance::HandleCreateSessionComplete( FName SessionName, bool bWasSuccessful)
{
	// TODO: Check bWasSuccessful to see if the session was created.

	UE_LOG(LogTemp, Warning, TEXT("Session: %d"), bWasSuccessful);

	// Deregister the event handler.
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(this->GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();

	UGameplayStatics::OpenLevel(this, "Lobby", true, "?listen");
	SetNotificationHandles();
	isInSession = true;


	Session->ClearOnCreateSessionCompleteDelegate_Handle(this->CreateSessionDelegateHandle);
	this->CreateSessionDelegateHandle.Reset();
}


void UEOSGameInstance::DestroySession()
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();

	DestroySessionDelegateHandle =
		Session->AddOnDestroySessionCompleteDelegate_Handle(FOnDestroySessionCompleteDelegate::CreateUObject(
			this,
			&ThisClass::HandleDestroySessionCompleted));

	if (!Session->DestroySession(FName(TEXT("MyLocalSessionName"))))
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to destroy session.")); // Log to the UE logs that we are trying to log in. 
		Session->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionDelegateHandle);
		DestroySessionDelegateHandle.Reset();
	}
}

void UEOSGameInstance::HandleDestroySessionCompleted(FName SessionName, bool bWasSuccessful)
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();

	

	if (bWasSuccessful)
	{
		//bSessionExists = false; // Mark that the session doesn't exist. This way next time BeginPlay is called a new session will be created. 
		UE_LOG(LogTemp, Warning, TEXT("Destroyed session succesfully."));
		UGameplayStatics::OpenLevel(this, FName("MainMenu"), true);
		isInSession = false;
		
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to destroy session."));
	}

	Session->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionDelegateHandle);
	DestroySessionDelegateHandle.Reset();
}

void UEOSGameInstance::OnSessionUserInviteAccepted(const bool bWasSuccess, const int32 ControllerId, TSharedPtr<const FUniqueNetId> UserId, const FOnlineSessionSearchResult& InviteResult)
{

	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();

	if (isInSession)
	{
		DestroySession();
	}

	if (bWasSuccess)
	{
		JoinSessionDelegateHandle =
			Session->AddOnJoinSessionCompleteDelegate_Handle(FOnJoinSessionCompleteDelegate::CreateUObject(
				this,
				&ThisClass::HandleJoinSessionCompleted));

		UE_LOG(LogTemp, Warning, TEXT("Joining session."));
		if (!Session->JoinSession(0, FName(TEXT("MyLocalSessionName")), InviteResult))
		{
			UE_LOG(LogTemp, Error, TEXT("Join session failed"));
		}
	}


	Session->ClearOnSessionUserInviteAcceptedDelegate_Handle(SessionInviteDelegateHandle);
	SessionInviteDelegateHandle.Reset();
}

void UEOSGameInstance::HandleJoinSessionCompleted(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();

	if (Result == EOnJoinSessionCompleteResult::Success)
	{
		FString ConnectionInfo = FString();
		Session->GetResolvedConnectString(SessionName, ConnectionInfo);

		if (!ConnectionInfo.IsEmpty())
		{
			if (APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(),0))
			{
				PC->ClientTravel(ConnectionInfo, ETravelType::TRAVEL_Absolute);
				SetNotificationHandles();
			}
		}

	}

	Session->ClearOnJoinSessionCompleteDelegate_Handle(JoinSessionDelegateHandle);
	JoinSessionDelegateHandle.Reset();
}

void UEOSGameInstance::HandleConnectToSessionCompleted(FName SessionName, const FUniqueNetId& UserId, bool bJoined)
{
	UE_LOG(LogTemp, Warning, TEXT("User joined Yay"));
}


void UEOSGameInstance::UploadPlayerData(USaveGame* SaveGame, FString FileName)
{

	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineIdentityPtr Identity = Subsystem->GetIdentityInterface();
	IOnlineUserCloudPtr Cloud = Subsystem->GetUserCloudInterface();

	this->WriteUserDataDelegateHandle =
		Cloud->AddOnWriteUserFileCompleteDelegate_Handle(FOnWriteUserFileComplete::FDelegate::CreateUObject(
			this,
			&ThisClass::HandleWriteFileComplete));

	TSharedPtr<const FUniqueNetId> UserID = Identity->GetUniquePlayerId(0).ToSharedRef();


	TArray<uint8> FileData;
	UGameplayStatics::SaveGameToMemory(SaveGame, FileData);

	Cloud->WriteUserFile(*UserID, FileName, FileData);

}


void UEOSGameInstance::HandleWriteFileComplete(bool bSuccess, const FUniqueNetId& userID, const FString& FileName)
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(this->GetWorld());
	IOnlineUserCloudPtr UserCloud = Subsystem->GetUserCloudInterface();

	UserCloud->ClearOnWriteUserFileCompleteDelegate_Handle(this->WriteUserDataDelegateHandle);
	this->WriteUserDataDelegateHandle.Reset();

	if (!bSuccess)
	{
		UE_LOG(LogTemp, Warning, TEXT("Data upload failed"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Data uploaded"));
	}
}

void UEOSGameInstance::EnumeratePlayerData()
{

	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineIdentityPtr Identity = Subsystem->GetIdentityInterface();
	IOnlineUserCloudPtr Cloud = Subsystem->GetUserCloudInterface();

	TSharedPtr<const FUniqueNetId> UserID = Identity->GetUniquePlayerId(0).ToSharedRef();

	this->EnumerateUserFilesDelegateHandle =
		Cloud->AddOnEnumerateUserFilesCompleteDelegate_Handle(FOnEnumerateUserFilesComplete::FDelegate::CreateUObject(
			this,
			&ThisClass::HandleEnumerateFileComplete));

	Cloud->EnumerateUserFiles(*UserID);

}

void UEOSGameInstance::HandleEnumerateFileComplete(bool bSuccess, const FUniqueNetId& userID)
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(this->GetWorld());
	IOnlineUserCloudPtr UserCloud = Subsystem->GetUserCloudInterface();
	UserCloud->ClearOnEnumerateUserFilesCompleteDelegate_Handle(this->EnumerateUserFilesDelegateHandle);
	this->EnumerateUserFilesDelegateHandle.Reset();

	IOnlineIdentityPtr Identity = Subsystem->GetIdentityInterface();
	TSharedPtr<const FUniqueNetId> UserID = Identity->GetUniquePlayerId(0).ToSharedRef();

	if (bSuccess)
	{
		TArray<FCloudFileHeader> Files;
		UserCloud->GetUserFileList(*UserID, Files);
		TArray<FString> FileNames;
		FileNames.Reserve(Files.Num() - 1);

		for (const auto& It : Files)
		{

			UE_LOG(LogTemp, Warning, TEXT("SaveFile Name %S"), FString(It.FileName));
			FileNames.Add(It.FileName);
			
		}
	}
}

void UEOSGameInstance::StartSession()
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();
	if (Session.IsValid())
	{
		Session->StartSession(FName(TEXT("MyLocalSessionName")));
	}
}

void UEOSGameInstance::EndSession()
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineSessionPtr Session = Subsystem->GetSessionInterface();
	if (Session.IsValid())
	{
		Session->EndSession(FName(TEXT("MyLocalSessionName")));
	}
}

void UEOSGameInstance::ReadPlayerData(FString FileName)
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineIdentityPtr Identity = Subsystem->GetIdentityInterface();
	IOnlineUserCloudPtr Cloud = Subsystem->GetUserCloudInterface();

	this->ReadUserFileDelegateHandle =
		Cloud->AddOnReadUserFileCompleteDelegate_Handle(FOnReadUserFileComplete::FDelegate::CreateUObject(
			this,
			&ThisClass::HandleReadFileComplete));

	TSharedPtr<const FUniqueNetId> UserID = Identity->GetUniquePlayerId(0).ToSharedRef();

	FString s = UserID->ToString();
	UE_LOG(LogTemp, Error, TEXT("UserID %s"), *s);

	if (!Cloud->ReadUserFile(*UserID, FileName))
	{
		UE_LOG(LogTemp, Error, TEXT("Cannot read User data"));
	}
}

void UEOSGameInstance::HandleReadFileComplete(bool bSuccess, const FUniqueNetId& userID, const FString& FileName)
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(this->GetWorld());
	IOnlineUserCloudPtr UserCloud = Subsystem->GetUserCloudInterface();
	IOnlineIdentityPtr Identity = Subsystem->GetIdentityInterface();

	UserCloud->ClearOnReadUserFileCompleteDelegate_Handle(this->ReadUserFileDelegateHandle);
	this->ReadUserFileDelegateHandle.Reset();

	TSharedPtr<const FUniqueNetId> UserID = Identity->GetUniquePlayerId(0).ToSharedRef();

	if (!bSuccess)
	{
		UE_LOG(LogTemp, Error, TEXT("Cannot read User data"));
	}
	else
	{
		TArray<uint8> FileContents;
		if (!UserCloud->GetFileContents(*UserID, FileName, FileContents))
		{
			UE_LOG(LogTemp, Error, TEXT("Cannot read User file"));
		}
		else
		{			
			this->SavedData = UGameplayStatics::LoadGameFromMemory(FileContents);

			if (SavedData == nullptr) {
				UE_LOG(LogTemp, Warning, TEXT("Data null"));
			}
			else {
				OnReadSaveDataSucces();
				UE_LOG(LogTemp, Warning, TEXT("Data readed"));
			}
		
		}
	}
}
