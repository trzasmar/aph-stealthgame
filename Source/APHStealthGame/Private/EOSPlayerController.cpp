// Fill out your copyright notice in the Description page of Project Settings.


#include "EOSPlayerController.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemUtils.h"
#include "OnlineSubsystemTypes.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "Kismet/GameplayStatics.h"
#include "EOSGameInstance.h"

AEOSPlayerController::AEOSPlayerController()
{

}

void AEOSPlayerController::BeginPlay()
{

    Super::BeginPlay();
    SetShowMouseCursor(true);

    FInputModeUIOnly inputMode;
    SetInputMode(inputMode);

    Login(); 
}

bool AEOSPlayerController::GetLoginStatus() const
{
    return UserLoggedIn;
}


void AEOSPlayerController::Login()
{
	IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
	IOnlineIdentityPtr Identity = Subsystem->GetIdentityInterface();

    FUniqueNetIdPtr NetId = Identity->GetUniquePlayerId(0);

    /*
    * BeginPlay fires even in map change. Don't try to log in, if alredy logged in
    */
    if (NetId != nullptr && Identity->GetLoginStatus(0) == ELoginStatus::LoggedIn)
    {
        return;
    }

    // Setup delegate for login complete
    LoginDelegateHandle = Identity->AddOnLoginCompleteDelegate_Handle(0,FOnLoginCompleteDelegate::CreateUObject(this,&ThisClass::HandleLoginCompleted));

    FString AuthType;
    FParse::Value(FCommandLine::Get(), TEXT("AUTH_TYPE="), AuthType);



    if (!AuthType.IsEmpty())
    {

        /*
        * Try Autologin player, from CLI
        */
        if (!Identity->AutoLogin(0))
        {
            UE_LOG(LogTemp, Warning, TEXT("Failed to login... "));

            Identity->ClearOnLoginCompleteDelegate_Handle(0, LoginDelegateHandle);
            LoginDelegateHandle.Reset();
        }
    }
    else
    {
        /*
        * Manual login or developer login
        */
        FOnlineAccountCredentials Credentials("AccountPortal", "", "");

        if (!Identity->Login(0, Credentials))
        {
            UE_LOG(LogTemp, Warning, TEXT("Failed to login... "));

            Identity->ClearOnLoginCompleteDelegate_Handle(0, LoginDelegateHandle);
            LoginDelegateHandle.Reset();
        }
    }

}

/*
* Delagated that is called after login is finished.
* Move player to main menu if login succesful
* Clears handles and delegates
*/
void AEOSPlayerController::HandleLoginCompleted(int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& UserId, const FString& Error)
{

    IOnlineSubsystem* Subsystem = Online::GetSubsystem(GetWorld());
    IOnlineIdentityPtr Identity = Subsystem->GetIdentityInterface();

    if (bWasSuccessful)
    {
        UE_LOG(LogTemp, Warning, TEXT("Login callback completed!"));;
       
        /*
        UEOSGameInstance* GameInstance = Cast<UEOSGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

        if (GameInstance != nullptr) {
           //GameInstance->LoggedInMainMenu();
        }
        */

        UserLoggedIn = true;
        UGameplayStatics::OpenLevel(this, FName("MainMenu"), true);

    }
    else //Login failed
    {

        UE_LOG(LogTemp, Warning, TEXT("EOS login failed."));
    }

    Identity->ClearOnLoginCompleteDelegate_Handle(LocalUserNum, LoginDelegateHandle);
    LoginDelegateHandle.Reset();
}

