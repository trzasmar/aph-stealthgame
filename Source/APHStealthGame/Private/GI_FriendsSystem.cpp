// Fill out your copyright notice in the Description page of Project Settings.


#include "GI_FriendsSystem.h"

#include "Interfaces/OnlineFriendsInterface.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "OnlineSubsystemUtils.h"
#include "OnlineSubsystem.h"


TArray<FString> UGI_FriendsSystem::GetDisplayNames()
{
	return DisplayNames;
}

void UGI_FriendsSystem::ShowInviteUI()
{
	class IOnlineSubsystem* OSS = IOnlineSubsystem::Get();

	if (OSS)
	{
		if (IOnlineExternalUIPtr UIPtr = OSS->GetExternalUIInterface()) {
			//UIPtr->ShowInviteUI(0, FName(TEXT("MyLocalSessionName")));
			UIPtr->ShowFriendsUI(0);
		}
	}
}

void UGI_FriendsSystem::SendInvite(int userID)
{
	class IOnlineSubsystem* OSS = IOnlineSubsystem::Get();
	IOnlineSessionPtr Session = OSS->GetSessionInterface();

	if (OSS)
	{
		if (IOnlineFriendsPtr FriendPtr = OSS->GetFriendsInterface())
		{

			//FriendPtr->SendInvite(0, UserIDs[userID].Get(), FString(""), FOnSendInviteComplete::CreateUObject(this, &UGI_FriendsSystem::OnSendInviteComplete));
		}

		Session->SendSessionInviteToFriend(0, FName(TEXT("MyLocalSessionName")), UserIDs[userID].Get());

	}
}

void UGI_FriendsSystem::OnSendInviteComplete(int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& FriendId, const FString& ListName, const FString& ErrorStr)
{
	UE_LOG(LogTemp, Warning, TEXT("Invitaion: %d, Error:%s "), bWasSuccessful, *FString(ErrorStr));
}


void UGI_FriendsSystem::GetListOfFriends()
{
	class IOnlineSubsystem* OSS = IOnlineSubsystem::Get();

	if (OSS)
	{
		if (IOnlineFriendsPtr FriendPtr = OSS->GetFriendsInterface())
		{
			FriendPtr->ReadFriendsList(0, ToString(EFriendsLists::Default), FOnReadFriendsListComplete::CreateUObject(this, &UGI_FriendsSystem::OnGetListOfFriendsComplete));
		}
	}
}


void UGI_FriendsSystem::OnGetListOfFriendsComplete(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName, const FString& ErrorStr)
{

	class IOnlineSubsystem* OSS = IOnlineSubsystem::Get();

	if (OSS)
	{
		if (IOnlineFriendsPtr FriendPtr = OSS->GetFriendsInterface())
		{
			TArray<TSharedRef<FOnlineFriend>> FriendsList;
			if (FriendPtr->GetFriendsList(0, ListName, FriendsList))
			{
				UE_LOG(LogTemp, Warning, TEXT("FriendListLenght: %d"), FriendsList.Num());
				for (TSharedRef<FOnlineFriend> Friend : FriendsList) 
				{
					DisplayNames.Add(Friend.Get().GetDisplayName());
					UserIDs.Add(Friend.Get().GetUserId());
				}
			}
		}
	}
}



