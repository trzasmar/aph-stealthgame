// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GI_FriendsSystem.generated.h"

/**
 * 
 */
UCLASS()
class APHSTEALTHGAME_API UGI_FriendsSystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

private:

	TArray<FString> DisplayNames;
	TArray<FUniqueNetIdRef> UserIDs;

public:

	UFUNCTION(BlueprintCallable)
	void GetListOfFriends();

	UFUNCTION(BlueprintCallable)
	TArray<FString> GetDisplayNames();

	UFUNCTION(BlueprintCallable)
	void SendInvite(int userID);

	UFUNCTION(BlueprintCallable)
	void ShowInviteUI();

	void OnGetListOfFriendsComplete(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName, const FString& ErrorStr);
	void OnSendInviteComplete( int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& FriendId, const FString& ListName, const FString& ErrorStr);

};
