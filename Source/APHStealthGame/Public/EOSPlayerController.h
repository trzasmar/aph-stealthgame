// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "EOSPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class APHSTEALTHGAME_API AEOSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	AEOSPlayerController();

	UFUNCTION(BlueprintCallable)
	void Login();

	UFUNCTION(BlueprintCallable)
	bool GetLoginStatus() const;

	void HandleLoginCompleted(int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& UserId, const FString& Error);

	FDelegateHandle LoginDelegateHandle;
	bool UserLoggedIn;

protected:

	virtual void BeginPlay();

};
