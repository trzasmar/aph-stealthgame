// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "EOSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class APHSTEALTHGAME_API UEOSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UEOSGameInstance();

	virtual void Init() override;

	UFUNCTION(BlueprintImplementableEvent)
	void LoggedInMainMenu();

	UFUNCTION(BlueprintImplementableEvent)
	void OnReadSaveDataSucces();

	UFUNCTION(BlueprintCallable)
	void CreateSession(bool isLAN);

	UFUNCTION(BlueprintCallable)
	void DestroySession();

	UFUNCTION(BlueprintCallable)
	void UploadPlayerData(USaveGame* SaveGame, FString FileName);

	UFUNCTION(BlueprintCallable)
	void EnumeratePlayerData();

	UFUNCTION(BlueprintCallable)
	void ReadPlayerData(FString FileName);

	UFUNCTION(BlueprintCallable)
	void StartSession();

	UFUNCTION(BlueprintCallable)
	void EndSession();

	void HandleDestroySessionCompleted(FName SessionName, bool bWasSuccessful);
	void HandleCreateSessionComplete(FName SessionName, bool bWasSuccessful);
	virtual void OnSessionUserInviteAccepted(const bool bWasSuccess,const int32 ControllerId, TSharedPtr< const FUniqueNetId > UserId,const FOnlineSessionSearchResult& InviteResult);
	void HandleJoinSessionCompleted(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
    void HandleConnectToSessionCompleted(FName SessionName, const FUniqueNetId& UserId, bool bJoined);

	void HandleWriteFileComplete(bool bSuccess, const FUniqueNetId& userID, const FString& FileName);
	void HandleEnumerateFileComplete(bool bSuccess, const FUniqueNetId& userID);
	void HandleReadFileComplete(bool bSuccess, const FUniqueNetId& userID, const FString& FileName);

protected:

	void SetNotificationHandles();
	bool isInSession;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USaveGame* SavedData;

	FDelegateHandle LoginDelegateHandle;
	FDelegateHandle CreateSessionDelegateHandle;
	FDelegateHandle DestroySessionDelegateHandle;
	FDelegateHandle SessionInviteDelegateHandle;
	FDelegateHandle JoinSessionDelegateHandle;
	FDelegateHandle ConnectToSessionHandle;
	FDelegateHandle WriteUserDataDelegateHandle;
	FDelegateHandle EnumerateUserFilesDelegateHandle;
	FDelegateHandle ReadUserFileDelegateHandle;


};
